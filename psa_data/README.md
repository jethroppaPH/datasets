The dataset presented has been sourced from the Philippine Statistics Authority (PSA) and is publicly available via this [link](http://openstat.psa.gov.ph/dataset/fisheries). Specifically, I have curated data from 2015 to 2017 pertaining to three prominent tuna species: bigeye tuna, skipjack, and yellowfin tuna. Notably, the dataset exclusively encompasses tuna production data from marine municipal fisheries.

This data set was used in my tutorial [here](https://jethroemmanuel.netlify.app/post/create-pie-charts-that-show-the-composition-of-tuna-landed-catch-on-a-map-in-r/).
